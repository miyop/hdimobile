//
//  String.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 29.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation
import UIKit

struct UIAlertControllerOptions {
    var title: String?
    var message: String?
    var actions: [(title: String , style: UIAlertAction.Style)]!
    var preferedStyle: UIAlertController.Style!
    
    init(title: String?, message: String?, actions: [(title: String , style: UIAlertAction.Style)]? = [("Tamam", .default)], preferedStyle: UIAlertController.Style!) {
        self.title = title
        self.message = message
        self.actions = actions
        self.preferedStyle = preferedStyle
    }
    init(preferedStyle: UIAlertController.Style) {
        self.preferedStyle = preferedStyle
        self.actions = [("Tamam", .default)]
    }
}
extension Date {
    
    func toString (format: String) -> String? {
        let DtFormat = DateFormatter()
        DtFormat.dateFormat = format
        return DtFormat.string(from: self)
    }
}

extension String {
    func toDate (format: String) -> Date? {
        let DtFormat = DateFormatter()
        DtFormat.dateFormat = format
        return DtFormat.date(from: self)
    }
    
    func toDateString (inputFormat: String, outputFormat:String) -> String? {
        if let date = toDate(format: inputFormat) {
            let DtFormat = DateFormatter()
            DtFormat.dateFormat = outputFormat
            return DtFormat.string(from: date)
        }
        return self
    }
    
    func temizle() -> String {
        
        return self.degistir("Ä", ikinci: "Ğ").degistir("Å", ikinci: "Ş").degistir("Ã", ikinci: "Ü").degistir("Ä°", ikinci: "İ").degistir("Ã", ikinci: "Ö").degistir("Ğ°", ikinci: "İ")
    }
    
    // "Ã" Ü
    // "Ã" Ö
    // Ä°ST.AVRUPA BÃLGE MÃDÃRLÃÄÃ
    
    func degistir(_ ilk:String, ikinci:String) -> String {
        return self.replacingOccurrences(of: ilk, with: ikinci, options: NSString.CompareOptions.caseInsensitive, range: nil)
    }
    
}


extension UITextField {
    
    func textFieldMaxLength(shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 12
        let currentString: NSString = self.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    // bu extension bize girilen değerin uzunluguna göre poliçe no yada vergi no oldugunu döndürür.
    
    func textFieldLength() -> String {
        
        if self.text?.count == 11 {
            return "tc"
        } else if self.text?.count == 10 {
            return "vr"
        }
        
        return ""
    }
}
extension UIColor {
    
    public class func hdiGreenColor()-> UIColor {
        return UIColor(hexString: "#258E59")
    }
    
    class func hdiDarkGreenColor()->UIColor {
        return UIColor(red: 0.0, green: 89/255.0, blue: 60/255.0, alpha: 1.0)
    }
    
    class func hdiLigtGreenColor()->UIColor {
        return UIColor(red: 182/255.0, green: 210/255.0, blue: 96/255.0, alpha: 1.0)
    }
    
}

extension UIViewController {
    func backButton() {
        navigationController?.navigationBar.topItem?.title = " " // x10 character
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func presentTransparentNavigationBar(status: Bool) {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = !status
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = !status
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        self.navigationController!.navigationBar.barTintColor = UIColor.hdiGreenColor()
    }
}
func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

extension UIViewController {
    
    
    func presentAlertWithOptions(_ title: String?, message: String?) {
        let alertViewControlller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
        alertViewControlller.addAction(doneAction)
        present(alertViewControlller, animated: true, completion: nil)
        
    }
    
    func presentAlertWithOptionsAndCompletionHandler(_ title: String?, message: String?, completion: @escaping () -> Void) {
        let alertViewControlller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Tamam", style: .cancel) { (didPress) -> Void in
            completion()
        }
        alertViewControlller.addAction(doneAction)
        present(alertViewControlller, animated: true, completion: nil)
        
    }
    
    /** Presents UIAlertViewController with given options. */
    func presentAlertWithOptionsAndCompletionHandler(options: UIAlertControllerOptions, completion: ((UIAlertAction) -> Void)?) {
        let alertViewControlller = UIAlertController(title: options.title, message: options.message , preferredStyle: options.preferedStyle)
        
        for action in options.actions {
            let alertAction = UIAlertAction(title: action.title, style: action.style, handler: completion)
            alertViewControlller.addAction(alertAction)
        }
        present(alertViewControlller, animated: true, completion: nil)
        
    }
    
    
    
    class func alertWithMessage(_ title:String?,message:String, handler:((UIAlertAction?) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler:handler))
        return alert
    }
    
}








