//
//  Request.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 29.08.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation
import Alamofire


extension Alamofire.Request  {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("=======================================")
        debugPrint(self)
        debugPrint("=======================================")
        #endif
        return self
    }
}
