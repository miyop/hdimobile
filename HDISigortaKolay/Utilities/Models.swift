//
//  Models.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 24.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//


import Foundation
import UIKit

struct PoliceBilgi {
    var expanded: Bool = false
    let tc: Int?
    let PoliceNo: Int?
    let YenilemeNo: Int?
    let ZeyilNo: Int?
    let Urun: String?
    let BaslangicTarihi: Int?
    let BitisTarihi: Int?
    let TanzimTarihi: Int?
    let Plaka: String?
    let AcenteKod: Int?
    let AcenteAd:String?
    let rizikoadres:String?
    let iptal:String?
    let msAd:String?
    let msSoyad:String?
}
struct AsistanBilgi {
    var expanded: Bool = false
    let AcenteKod: Int?
    let AcenteAd:String?
    let PoliceNo: Int?
    let YenilemeNo: Int?
    let ZeyilNo: Int?
    let Ad:String?
    let SoyAd:String?
    let Adres:String?
    let TcKimlikNo: Int?
    let VergiKimlikNo: String?
    let Plaka: String?
    let Marka: String?
    let Model: String?
    let ModelYil: Int?
    let AracListeKod:Int?
    let BaslangicTarihi: Int?
    let BitisTarihi: Int?
    let TanzimTarihi: Int?
    let IptalDurumu:String?
    let Urun: String?
    let Kontrat: String?
    let WebServisKodu: String?
    let Arıza: String?
    let Prim: Float?
}
struct Photo {
    
    let caption:String
    let imageName:String
    let titletext:String
    
    
    init(caption:String, imageName:String, titletext:String){
        self.caption = caption
        self.imageName = imageName
        self.titletext = titletext
    }
    
    public struct Item {
        public var name: String
        public var detail: String
        
        public init(name: String, detail: String) {
            self.name = name
            self.detail = detail
        }
    }
    
}
struct HasarBilgi {
    let policeBilgileri : PoliceBilgileri?
    let dosyaBilgileri : DosyaBilgileri?
    let eksikEvraklar : EksikEvraklar?
    let ekBilgiler : EkBilgiler?
    
    enum CodingKeys: String, CodingKey {
        case policeBilgileri = "PoliceBilgileri"
        case dosyaBilgileri = "DosyaBilgileri"
        case eksikEvraklar = "EksikEvraklar"
        case ekBilgiler = "EkBilgiler"
    }
}

struct PoliceBilgileri : Codable {
    let policeNo : Int?
    let tecditNo : Int?
    let turu : String?
    let acenteNo : String?
    let acenteUnvani : String?
    let musteriAdsoy : String?
    let baslangicTarihi : Date?
    let bitisTarihi : Date?
    
    enum CodingKeys: String, CodingKey {
        case policeNo = "PoliceNo"
        case tecditNo = "TecditNo"
        case turu = "Turu"
        case acenteNo = "AcenteNo"
        case acenteUnvani = "AcenteUnvani"
        case musteriAdsoy = "MusteriAdsoy"
        case baslangicTarihi = "BaslangicTarihi"
        case bitisTarihi = "BitisTarihi"
    }
}

struct DosyaBilgileri : Codable {
    let dosyaNo : Int?
    let ekNo : Int?
    let dosyaDurumu : String?
    let hasarTarihi : Date?
    let ihbarTarihi : Date?
    let hasarSahibi : String?
    let hasarSahibiKimNo : Int?
    let karsiAracPlaka : String?
    let marka : String?
    let osiKodu : Int?
    let tahminiHasarTutari : Float?
    let gercekHasarTutari : Float?
    let odemeTarihi : Date?
    let yedekAlan3 : Float?
    let sorumluKodu : String?
    let sorumluMail : String?
    
    enum CodingKeys: String, CodingKey {
        case dosyaNo = "DosyaNo"
        case ekNo = "EkNo"
        case dosyaDurumu = "DosyaDurumu"
        case hasarTarihi = "HasarTarihi"
        case ihbarTarihi = "IhbarTarihi"
        case hasarSahibi = "HasarSahibi"
        case hasarSahibiKimNo = "HasarSahibiKimNo"
        case karsiAracPlaka = "KarsiAracPlaka"
        case marka = "Marka"
        case osiKodu = "osiKodu"
        case tahminiHasarTutari = "TahminiHasarTutari"
        case gercekHasarTutari = "GercekHasarTutari"
        case odemeTarihi = "OdemeTarihi"
        case yedekAlan3 = "YedekAlan3"
        case sorumluKodu = "sorumluKodu"
        case sorumluMail = "sorumluMail"
    }
}

struct EksikEvraklar : Codable {
    let evrKod : Int?
    let evrAdi : String?
    
    enum CodingKeys: String, CodingKey {
        case evrKod = "evrKod"
        case evrAdi = "evrAdi"
    }
}

struct EkBilgiler : Codable {
    let blgAdi : String?
    
    enum CodingKeys: String, CodingKey {
        case blgAdi = "blgAdi"
    }
}

struct policeSection {
    public var genre: String!
    public var icerik: PoliceBilgi!
    
    public  var height: CGFloat!
    
    init(genre: String, icerik: PoliceBilgi, height: CGFloat) {
        self.genre = genre
        self.icerik = icerik
        
        self.height = height
    }
}

struct asistanSection {
    public var genre: String!
    
    public var asistanicerik : AsistanBilgi!
    public  var height: CGFloat!
    
    init(genre: String, asistanicerik : AsistanBilgi, height: CGFloat) {
        self.genre = genre
        self.asistanicerik = asistanicerik
        self.height = height
    }
}

