//
//  GlobalVariables.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 27.06.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation


private let _shared = GlobalVariables()
// Yeni Oluşturduğum Genel Değişken
private let _sharedList = GlobalVariables()

class GlobalVariables : NSObject {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalVariables {
        return _shared
    }
    
    // Yeni Oluşturduğum Genel Değişken
    class var sharedList : GlobalVariables {
        return _sharedList
    }
    
    
    var policebilgi: [PoliceBilgi]? = nil
    var policeListeleme : [PoliceBilgi]? = nil
    var asistanPoliceListeleme: [AsistanBilgi]? = nil
    var hasarSorgu : [HasarBilgi]? =  nil
}






