//
//  AlertFunctions.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 29.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import UIKit
import Foundation

private let _shared = AlertFunctions()
class AlertFunctions : NSObject {
    
    // MARK: - SHARED INSTANCE
    class var messageType : AlertFunctions {
        
        return _shared
    }
    
    func showYesNoAlert(_ titleMessage: String, bodyMessage: String, _ yes: @escaping () -> Void, no: @escaping () -> Void){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (action: UIAlertAction!) in
            yes()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(action: UIAlertAction!) in
            no()
        }))
        
        getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showOKAlert(_ titleMessage: String, bodyMessage: String){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func getTopController() -> UIViewController{
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
}


