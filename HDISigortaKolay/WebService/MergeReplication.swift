//
//  MergeReplication.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 23.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation
import UIKit
class MergeReplication {
    
    var adres = ""
    var policeListelemeAdres = ""
    var asistanAdres = ""
    var hasarAdres = ""
    var result = WebServicesReader()
    
    func PoliceBilgiGetir( _ policeno : Int , tckimlik :Int , type : String, cevap: @escaping ([PoliceBilgi]?, Error?) -> Void) {
        
        // adres = "https://srv.hdisigorta.com.tr:1443/cgi-bin/PINKP2.pgm?&id=\(tckimlik)&pl=\(policeno)"//
        // https://srv.hdisigorta.com.tr:1443/cgi-bin/PINKP2.pgm?&id=2960011685&pl=320101010298
        //http://srv.hdisigorta.com.tr:1080/cgi-bin/WBYASAYANP.pgm?u=ASLNUIVR&p=9mQ3d52w&tc=2960011685&pn=320101010298
        
        adres = "http://srv.hdisigorta.com.tr:1080/cgi-bin/WBYASAYANP.pgm?u=ASLNUIVR&p=9mQ3d52w&\(type)=\(tckimlik)&pn=\(policeno)"
        
        result.TCKimlikServiceReader(adres) { (policem, error)  in
            cevap(policem, error)
        }
    }
    
    
    func PoliceListeleme( _ tckVr: Int , type : String, cevap: @escaping ([PoliceBilgi]?, Error?) -> Void ) {
        
        policeListelemeAdres =  "http://srv.hdisigorta.com.tr:1080/cgi-bin/WBYASAYANP.pgm?u=ASLNUIVR&p=9mQ3d52w&\(type)=\(tckVr)"
        
        result.PoliceListingServiceReader(policeListelemeAdres) { (policem, error) in
            cevap(policem, error)
        }
        
    }
    
    func AsistanPoliceListeleme( _ tckVr: Int , type : String, cevap: @escaping ([AsistanBilgi]?, Error?) -> Void ) {
        
        asistanAdres = "http://srv.hdisigorta.com.tr:1080/cgi-bin/WBASLINE.pgm?u=ASLNU&p=9mQ3d52w&&\(type)=\(tckVr)"
        
        
        result.PoliceAssistantQueryServiceReader(asistanAdres) { (policem, error) in
            cevap(policem, error)
        }
    }
    
    func PoliceBilgiMailGönderme (_ params:(AsistanBilgi?) , MüsteriTelefonNo: String, cevap : @escaping ( Bool) -> Void) {
        var param = "<strong><font color=\"green\">Müşteri Ad-Soyad/Ünvan:\(params!.Ad! + (params?.SoyAd!)!)</font></strong>"
        param += "<br><strong><font color=\"green\">T.C Kimlik/Vergi No.:\(params!.TcKimlikNo!)</font></strong>"
        param += "<br><strong><font color=\"green\">Telefon Numarası:\(MüsteriTelefonNo)</font></strong>"
        param += "<br><strong><font color=\"green\">Police No:\(params!.PoliceNo!)</font></strong>"
        param += "<br><strong><font color=\"green\">Acente Kodu:\(params!.AcenteKod!)</font></strong>"
        param += "<br><strong><font color=\"green\">Acente Adı:\(params!.AcenteAd!)</font></strong>"
        param += "<br><strong><font color=\"green\">Başlangıç Tarihi:\(params!.BaslangicTarihi!)</font></strong>"
        param += "<br><strong><font color=\"green\">Bitiş Tarihi:\(params!.BitisTarihi!)</font></strong>"
        param += "<br><strong><font color=\"green\">Plaka:\(params!.Plaka!)</font></strong>"
        param += "<br><strong><font color=\"green\">İptal Durumu:\(params!.IptalDurumu!)</font></strong>"
        let paramUtf8 = param.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        // http://web.hdisigorta.com.tr/cgi-bin/SYLON-MAIL.php?android=isMail&tip=9&detay=testasdas
        result.HelpMailPost(adres:"http://web.hdisigorta.com.tr/cgi-bin/SYLON-MAIL.php?ios=isMail&tip=10&detay=" + paramUtf8! ) {(commentpost) -> Void in
            cevap(commentpost)
        }
        
    }
   
    func HasarBilgiListeleme( _ tckVr: Int , dosyaNo : Int, cevap: @escaping (HasarBilgi?, Error?) -> Void ) {
        
        hasarAdres = "http://srv.hdisigorta.com.tr:1080/cgi-bin/SYLON.pgm?xmlData=%3Chead%3E%3Cuser%3EWS_MOBIL%3C/user%3E%3Cpwd%3E4AKDKepmwX%3C/pwd%3E%3CUygulama%3EUYG002%3C/Uygulama%3E%3CTcveyaVergiNo%3E\(tckVr)%3C/TcveyaVergiNo%3E%3CDosyaNo%3E\(dosyaNo)%3C/DosyaNo%3E%3C/head%3E"
        
       
        result.HasarDamageQueryServiceReader(hasarAdres) { (hasar, error) in
            cevap(hasar,error)
        }
        
       
    }
    
    
}


