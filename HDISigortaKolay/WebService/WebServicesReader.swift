//
//  WebServicesReader.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 23.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//
import UIKit
import Foundation
import SWXMLHash
import SwiftyJSON
import Alamofire

class WebServicesReader {
    
    init() {}
    
    // MARK: TC Kimlik (ilk login sayfasında) servisini okur ve tum bilgileri array icersine doldurur.
    func TCKimlikServiceReader (_ adres : String, completion: @escaping ( [PoliceBilgi]?, Error? ) -> Void) {
        
        Alamofire.request(adres).responseString { response in
            if let xmlString = response.result.value {
                let xml = SWXMLHash.parse(xmlString)
                let result = xml["HDICPOLS"]
                
                
                var tcNo = ""
                if let tc = result["tc"].element?.text {
                    tcNo = tc
                }
                
                var policeler: [PoliceBilgi] = []
                for police in result["Police"].all {
                    let policeBilgi = PoliceBilgi (
                        expanded: false
                        
                        , tc: Int(tcNo)
                        , PoliceNo: Int(police["PoliceNo"].element!.text) ?? -1
                        , YenilemeNo: Int(police["YenilemeNo"].element!.text) ?? -1
                        , ZeyilNo: Int(police["ZeyilNo"].element!.text) ?? -1
                        , Urun: police["Urun"].element?.text.temizle() ?? ""
                        , BaslangicTarihi: Int(police["BaslangicTarihi"].element!.text) ?? -1
                        , BitisTarihi: Int(police["BitisTarihi"].element!.text) ?? -1
                        , TanzimTarihi: Int(police["TanzimTarihi"].element!.text) ?? -1
                        //, Plaka: Int(police["Plaka"].element!.text) ?? -1
                        , Plaka: police["Plaka"].element?.text ?? ""
                        , AcenteKod: Int(police["AcenteKod"].element!.text) ?? -1
                        , AcenteAd: police["AcenteAd"].element?.text.temizle() ?? ""
                        , rizikoadres: police["rizikoadres"].element?.text.temizle() ?? ""
                        , iptal: police["iptal"].element?.text.temizle() ?? ""
                        , msAd: police["msAd"].element?.text.temizle() ?? ""
                        , msSoyad: police["msSoyad"].element?.text.temizle() ?? ""
                        
                    )
                    policeler.append(policeBilgi)
                    
                }
                
                completion(policeler, nil)
                
            } else{
                print(response.result.error!.localizedDescription)
                completion(nil, response.result.error!)
            }
        }
        
    }
    
    
    //---------------------------------------------------------------------------------------------------------//
    // MARK: Poliçe listeleme servisini okur ve tc kimliğe ait tum poliçe bilgilerini array icersine doldurur. //
    
    func PoliceListingServiceReader (_ adres : String, completion: @escaping ( [PoliceBilgi ]?, Error? ) -> Void) {
        
        Alamofire.request(adres).responseString { response in
            if let xmlString = response.result.value {
                let xml = SWXMLHash.parse(xmlString)
                let resultpolice = xml["HDICPOLS"]
                
                var policelerListe: [PoliceBilgi ] = []
                for police in resultpolice["Police"].all {
                    let policeListesi = PoliceBilgi  (
                        expanded: false
                        
                        , tc: nil // burada gerek yok
                        , PoliceNo: Int(police["PoliceNo"].element!.text) ?? -1
                        , YenilemeNo: Int(police["YenilemeNo"].element!.text) ?? -1
                        , ZeyilNo: Int(police["ZeyilNo"].element!.text) ?? -1
                        , Urun: police["Urun"].element?.text.temizle() ?? ""
                        , BaslangicTarihi: Int(police["BaslangicTarihi"].element!.text) ?? -1
                        , BitisTarihi: Int(police["BitisTarihi"].element!.text) ?? -1
                        , TanzimTarihi: Int(police["TanzimTarihi"].element!.text) ?? -1
                        , Plaka: police["Plaka"].element?.text ?? ""
                        , AcenteKod: Int(police["AcenteKod"].element!.text) ?? -1
                        , AcenteAd: police["AcenteAd"].element?.text.temizle() ?? ""
                        , rizikoadres: police["rizikoadres"].element?.text.temizle() ?? ""
                        , iptal: police["iptal"].element?.text.temizle() ?? ""
                        , msAd: police["msAd"].element?.text.temizle() ?? ""
                        , msSoyad: police["msSoyad"].element?.text.temizle() ?? ""
                        
                    )
                    policelerListe.append(policeListesi)
                    
                }
                
                completion(policelerListe, nil)
                
            } else{
                print(response.result.error!.localizedDescription)
                completion(nil, response.result.error!)
            }
        }
        
    }
    
    // MARK: Asistan teminatı olan poliçe listeleme servisini okur ve tum poliçe bilgilerini array icersine doldurur. //
    
    func PoliceAssistantQueryServiceReader (_ adres : String, completion: @escaping ( [AsistanBilgi ]?, Error? ) -> Void) {
        
        Alamofire.request(adres).responseString { response in
            if let xmlString = response.result.value {
                let xml = SWXMLHash.parse(xmlString)
                let resultpolice = xml["HDICPOLS"]
                
                var asistanListe: [AsistanBilgi] = []
                
                for police in resultpolice["Police"].all {
                    let AsistanPoliceListesi = AsistanBilgi  (
                        expanded: false
                        , AcenteKod: Int(police["AcenteKod"].element!.text) ?? -1
                        , AcenteAd: police["AcenteAd"].element?.text.temizle() ?? ""
                        , PoliceNo: Int(police["PoliceNo"].element!.text) ?? -1
                        , YenilemeNo: Int(police["YenilemeNo"].element!.text) ?? -1
                        , ZeyilNo: Int(police["ZeyilNo"].element!.text) ?? -1
                        , Ad: police["Ad"].element?.text.temizle() ?? ""
                        , SoyAd: police["SoyAd"].element?.text.temizle() ?? ""
                        , Adres: police["Adres"].element?.text.temizle() ?? ""
                        , TcKimlikNo: Int(police["TcKimlikNo"].element!.text) ?? -1
                        , VergiKimlikNo: police["VergiKimlikNo"].element?.text.temizle() ?? ""
                        , Plaka: police["Plaka"].element?.text
                        , Marka: police["Marka"].element?.text.temizle() ?? ""
                        , Model: police["Model"].element?.text.temizle() ?? ""
                        , ModelYil: Int(police["ModelYil"].element!.text) ?? -1
                        , AracListeKod: Int(police["AracListeKod"].element!.text) ?? -1
                        , BaslangicTarihi: Int(police["BaslangicTarihi"].element!.text) ?? -1
                        , BitisTarihi: Int(police["BitisTarihi"].element!.text) ?? -1
                        , TanzimTarihi: Int(police["TanzimTarihi"].element!.text) ?? -1
                        , IptalDurumu: police["IptalDurumu"].element?.text.temizle() ?? ""
                        , Urun: police["Urun"].element?.text.temizle() ?? ""
                        , Kontrat: police["Kontrat"].element?.text.temizle() ?? ""
                        , WebServisKodu: police["WebServisKodu"].element?.text.temizle() ?? ""
                        , Arıza: police["Arıza"].element?.text.temizle() ?? ""
                        , Prim: Float(police["Prim"].element!.text) ?? -1.0
                        
                    )
                    asistanListe.append(AsistanPoliceListesi)
                }
                
                completion(asistanListe, nil)
            } else{
                print(response.result.error!.localizedDescription)
                completion(nil, response.result.error!)
            }
        }
        
    }
    
    // MARK:  Yardım sayfası mail gönderme modülü //
    
    func HelpMailPost(adres : String ,answer: @escaping (Bool) ->  Void) {
        
        Alamofire.request(adres).debugLog()
            .responseData { (response) in
                if response.result.value != nil {
                    answer(true)
                } else {
                    answer(false)
                }
        }
        
    }
    
    
    // MARK:   Hasar Sorgulama sayfası modülü //
    func HasarDamageQueryServiceReader (_ adres : String, completion: @escaping (HasarBilgi?, Error? ) -> Void) {
        Alamofire.request(adres).responseString { response in
            if let xmlString = response.result.value?.degistir("İ", ikinci: "I") {
                
                let xml = SWXMLHash.parse(xmlString)
                let result = xml["HDISIGORTA"]["HASAR"]
                let resultPoliceBilgi = result["PoliceBilgileri"]
                let resultDosyaBilgi = result["DosyaBilgileri"]
                let resultEvrakBilgi = result["EksikEvraklar"]
                let resultEkBilgi = result["EkBilgiler"]
                
                let policeBilgi = PoliceBilgileri (
                    policeNo : Int(resultPoliceBilgi["PoliceNo"].element!.text) ?? -1
                    ,tecditNo : Int(resultPoliceBilgi["TecditNo"].element!.text) ?? -1
                    ,turu : resultPoliceBilgi["Turu"].element?.text.temizle() ?? ""
                    ,acenteNo : resultPoliceBilgi["AcenteNo"].element?.text.temizle() ?? ""
                    ,acenteUnvani : resultPoliceBilgi["AcenteUnvani"].element?.text.temizle() ?? ""
                    ,musteriAdsoy : resultPoliceBilgi["MusteriAdsoy"].element?.text.temizle() ?? ""
                    ,baslangicTarihi : resultPoliceBilgi["BaslangicTarihi"].element!.text.toDate(format: "dd/mm/yyyy")
                    ,bitisTarihi : resultPoliceBilgi["BitisTarihi"].element!.text.toDate(format: "dd/mm/yyyy")
                )
                
                let dosyaBilgi = DosyaBilgileri (
                    dosyaNo : Int(resultDosyaBilgi["DosyaNo"].element!.text) ?? -1
                    ,ekNo : Int(resultDosyaBilgi["EkNo"].element!.text) ?? -1
                    ,dosyaDurumu : resultDosyaBilgi["DosyaDurumu"].element?.text.temizle() ?? ""
                    ,hasarTarihi : resultDosyaBilgi["HasarTarihi"].element!.text.toDate(format: "dd/mm/yyyy")
                    ,ihbarTarihi : resultDosyaBilgi["IhbarTarihi"].element!.text.toDate(format: "dd/mm/yyyy")
                    ,hasarSahibi : resultDosyaBilgi["HasarSahibi"].element?.text.temizle() ?? ""
                    ,hasarSahibiKimNo :Int(resultDosyaBilgi["HasarSahibiKimNo"].element!.text) ?? -1
                    ,karsiAracPlaka : resultDosyaBilgi["KarsiAracPlaka"].element!.text
                    ,marka : resultDosyaBilgi["Marka"].element?.text.temizle() ?? ""
                    ,osiKodu : Int(resultDosyaBilgi["osiKodu"].element!.text) ?? -1
                    ,tahminiHasarTutari : Float(resultDosyaBilgi["TahminiHasarTutari"].element!.text) ?? -1
                    ,gercekHasarTutari : Float(resultDosyaBilgi["GercekHasarTutari"].element!.text) ?? -1
                    ,odemeTarihi : resultDosyaBilgi["OdemeTarihi"].element!.text.toDate(format: "dd mm yyyy")
                    ,yedekAlan3 : Float(resultDosyaBilgi["YedekAlan3"].element!.text) ?? -1
                    ,sorumluKodu : resultDosyaBilgi["sorumluKodu"].element?.text.temizle() ?? ""
                    ,sorumluMail : resultDosyaBilgi["sorumluMail"].element?.text.temizle() ?? ""
                )
                
                var evrakBilgi : EksikEvraklar?
                if resultEvrakBilgi["evrKod"].element != nil {
                    evrakBilgi =  EksikEvraklar (
                         evrKod : Int(resultEvrakBilgi["evrKod"].element!.text) ?? -1
                        ,evrAdi : resultEvrakBilgi["evrAdi"].element?.text.temizle() ?? ""
                    )
                }
                
                var ekBilgi : EkBilgiler?
                if resultEkBilgi["blgAdi"].element != nil {
                    ekBilgi =  EkBilgiler (
                        blgAdi : resultEkBilgi["blgAdi"].element?.text.temizle() ?? ""
                    )
                }
                
                let hasar = HasarBilgi (
                    policeBilgileri: policeBilgi
                    , dosyaBilgileri: dosyaBilgi
                    , eksikEvraklar: evrakBilgi
                    , ekBilgiler: ekBilgi
                )
                
                completion(hasar, nil)
            
            } else{
                print(response.result.error!.localizedDescription)
                completion(nil, response.result.error!)
            }
        }
    }
}
