//
//  ProductCollectionViewCell.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 25.06.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
        
}
