//
//  CustomCell.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 11.07.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//


import UIKit

class CustomCell: UITableViewCell {
    
    let view = PoliceRowCell()
    
    // MARK: Initalizers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add subview
        contentView.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
