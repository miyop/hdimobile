//
//  PoliceRowCell.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 24.07.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import UIKit

class PoliceRowCell: UIView {
    
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var nameImage: UIImageView!
    @IBOutlet weak var nameUrun: UILabel!
    @IBOutlet weak var namePolice: UILabel!
    @IBOutlet weak var nameAcente: UILabel!
    @IBOutlet weak var nameAcenteAd: UILabel!
    @IBOutlet weak var nameBsTarih: UILabel!
    @IBOutlet weak var nameBtTarih: UILabel!
    @IBOutlet weak var namePolDurm: UILabel!
    @IBOutlet weak var nameZeyil: UILabel!
 
    
    @IBOutlet weak var nameAdresPlaka: UILabel!
    
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func formatSelf() {
        self.backgroundColor = UIColor.blue
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("PoliceRowCell", owner: self, options: nil)
        contentView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: 383, height: 315)
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(contentView)
    }

    @IBAction func btnMail(_ sender: UIButton) {
        print("Burada oluyor..")
    }
    
}

