//
//  CustomerCellVC+SendEmailHandlerViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 7.08.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import UIKit
import MessageUI
//
//extension CustomCellViewController: MFMailComposeViewControllerDelegate {
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    func OnSendErrorReport(sender: UIButton) {
//        //Check to see the device can send email.
//        if MFMailComposeViewController.canSendMail() {
//            print("Can send email.")
//            let mailComposer = MFMailComposeViewController()
//            mailComposer.mailComposeDelegate = self
//
////            let report = GlobalVariables.reportMail.self
////            mailComposer.setToRecipients([report.ToMail.rawValue])
////            mailComposer.setCcRecipients([report.CcMail.rawValue])
////
////            //Set the subject and message of the email
////            let companyname = OrcaCompanyFunctions.shared.getActiveCompany()!.companyname!
////            let user =  GlobalVariables.shared.activeUser!
////
////            mailComposer.setSubject("Report for " + companyname + "/" + user.name!)
////
//            var body = "I get the following error:\n\n"
////            body += "Report for Orca Pos " + companyname + " (\(user.code!))"
////            body += "/" + user.name!
////            body += "\nApp Version: " + GlobalFunctions.shared.version()
////            body += "\nGuid ID: " + GlobalVariables.shared.guidID
////            body += "\nDevice ID: " + NoseriesFunctions.shared.getNoseries()!.seriescode!
////            body += "\nDevice Name: " + GlobalVariables.shared.deviceName
////            body += "\nSystem: " + UIDevice.current.model + "/" + UIDevice.current.systemName + " " + UIDevice.current.systemVersion
//
//            mailComposer.setMessageBody(body, isHTML: false)
//
//            self.present(mailComposer, animated: true, completion: nil)
//        } else {
//            AlertFunctions.messageType.showYesNoAlert("Email Error", bodyMessage: "Set up an e-mail account immediately to send a diagnostic report?\nAccounts & Password -> Add Account", {
//                if let url = URL(string:"App-prefs:root=ACCOUNT_SETTINGS") {
//                    if UIApplication.shared.canOpenURL(url) {
//                        if #available(iOS 10.0, *) {
//                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                        } else {
//                            UIApplication.shared.openURL(url)
//                        }
//                    }
//                }
//            }, no: {
//
//            })
//        }
//    }
//}
//
