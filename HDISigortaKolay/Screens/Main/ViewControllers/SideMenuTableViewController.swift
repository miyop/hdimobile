//
//  SideMenuTableViewController.swift
//  SideMenu
//
//  Created by Jon Kent on 4/5/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import SideMenu

class SideMenuTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        
       // self.tableView.register(UITableViewVibrantCell.self, forCellReuseIdentifier: "Cell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh cell blur effect in case it changed
        tableView.reloadData()
        
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        
        // Set up a cool background image for demo purposes
//        let imageView = UIImageView(image: UIImage(named: "GreenBanner"))
//        imageView.contentMode = .scaleAspectFit
//        imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
//        imageView.backgroundColor = UIColor.black
//        tableView.backgroundView = imageView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     // bir linke gidecekse burada yönlendir. Sayfaya gidecekse storyboarddan sürükle
        switch indexPath.row {
        case 0:
            //
            break
        case 1:
            //
            break
        case 2:
            //
            break
        default: break
        }
        
        
    }
    
   // override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     //   let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell
        
      //  cell.blurEffectStyle = SideMenuManager.default.menuBlurEffectStyle
        
      //  return cell
    //}
    
}
