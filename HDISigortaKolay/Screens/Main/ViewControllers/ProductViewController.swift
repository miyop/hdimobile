//
//  ProductViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 25.06.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//


import UIKit
import SideMenu

class ProductViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerPreviewingDelegate { //UICollectionViewDelegateFlowLayout
    
    var segueIdentifiers = ["toPoliceListingViewController", "toDamageInformationViewController", "", "toDamageInformationViewController", ""]
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var productCollectionView: UICollectionView!
    lazy var photos:[Photo] = {
        return [
    
            Photo(caption : "POLİÇELERİM", imageName:"Policelerim", titletext:"Güncel poliçelerinizi görün."),
            Photo(caption : "HASAR SORGULAMA", imageName:"HasarSorgulama", titletext:"Hasar dosyanızı görüntüleyin."),
            Photo(caption : "YARDIM HİZMETLERİ", imageName:"YardımHizmetleri", titletext:"Poliçenizden yardım hizmeti talep edin."),
            Photo(caption : "HDI ROBOT", imageName:"hdibot", titletext:"HDI Robotu HADi'den çevrimiçi hizmet alın.."),
            Photo(caption : "BİZE ULAŞIN", imageName:"BizeUlasın", titletext:"Farklı platformlardan bize ulaşın."),
        ]
    }()
    
    // let images = ["Policelerim", "HasarSorgulama" , "YardımHizmetleri", "hdibot" ,"BizeUlasın"]
    // let titles = ["POLİÇELERİM" , "HASAR SORGULAMA", "YARDIM HİZMETLERİ" , "HDI ROBOT", "BİZE ULAŞIN" ]
    // let titletext = ["Güncel poliçelerinizi görün." , "Hasar dosyanızı görüntüleyin." ," Poliçenizden yardım hizmeti talep edin.","HDI Robotu HADi'den çevrimiçi hizmet alın." ,"Farklı platformlardan bize ulaşın."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presentTransparentNavigationBar(status: true)
        
        self.productCollectionView.delegate = self
        self.productCollectionView.dataSource = self
        
         //Register for 3D Touch
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: productCollectionView)
            
         }
        
        let pb = GlobalVariables.shared.policebilgi![0]
        
        customerName.text = "Hoşgeldiniz " + pb.msAd!.temizle() + "" + pb.msSoyad!.temizle()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let CellWidth: Float = 190
        let CellCount: Float = Float(photos.count)
        let CellSpacing: Float = 10
        
        let totalCellWidth = CellWidth * CellCount
        let totalSpacingWidth = CellSpacing * (CellCount - 1)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets.init(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    //override func performSegue(withIdentifier identifier: String, sender: Any?) {
      //  if( identifier == "toProductListingViewController") {
        //   let vc = sender as! ProductListingTableViewController
            
          //let selectedCell = productCollectionView.indexPathsForSelectedItems![0] //sender as! ProductCollectionViewCell
            //let indexPath = productCollectionView.indexPath(for: selectedCell)
            
       //}
   //}

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ProductCollectionViewCell
        let photo = photos[indexPath.row]
        cell.imageView.image = UIImage(named: photo.imageName)
        cell.titleLabel.text = photo.caption
        cell.textLabel.text = photo.titletext
       // cell.textLabel.text = titletext[indexPath.row]
        
        
        cell.layer.masksToBounds = true
        //cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 0.5
        //cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        //cell.layer.shadowOpacity = 1
        cell.layer.borderColor = UIColor.black.cgColor
        
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
          let customcellViewControlloler = CustomCellViewController()
          let damageInformationViewController = DamageInformationViewController()

        if indexPath.row == 0 {
            self.navigationController?.pushViewController(customcellViewControlloler, animated: true)
            customcellViewControlloler.selectedValue = "Police"
            
        } else if indexPath.row == 2 {
             self.navigationController?.pushViewController(customcellViewControlloler, animated: true)
           customcellViewControlloler.selectedValue = "Yardım"
        }
         
        else if indexPath.row == 1 {
            damageInformationViewController.selectedValue = "Hasar"
            //iki biçimde seque yapabilirim
            //1.Push ile kendin oluşturup kod ile seque yi ama storyboarda idetifier tanımlayarak
            //2.İki ekran arasında çizgi çekip identifier vererek
            
//            self.performSegue(withIdentifier: "toDamageViewController", sender: nil)
            
            let next = storyboard?.instantiateViewController(withIdentifier: "vcDamage") as! DamageInformationViewController
            self.navigationController?.pushViewController(next, animated: true)
        }
        
    }
    
    // peek
    func previewingContext (_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = productCollectionView?.indexPathForItem(at: location) else { return nil }
        guard let cell = productCollectionView?.cellForItem(at: indexPath) else { return nil }
        guard let detailVC = storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController else { return nil }
    
        let basılanphoto = photos[indexPath.row]
        detailVC.photoX = basılanphoto
    
        detailVC.preferredContentSize = CGSize(width: 0.0, height: 300)
        previewingContext.sourceRect = cell.frame

        return detailVC
    }
        
    // pop
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
        
        if( segue.identifier == "mySegue") {
            
          
        }
    }
    
}




