//
//  PhotoViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 2.07.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation

import UIKit

class PhotoViewController: UIViewController {

    
    @IBOutlet weak var image3d: UIImageView!
    
    @IBOutlet weak var captionLabel: UILabel!
    
    var photoX:Photo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let photo = photoX {
          image3d.image = UIImage(named: photo.imageName)
          captionLabel.text = photo.caption
          title = photo.titletext
            
            }
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func previewActionItems() -> [UIPreviewActionItem] {
        
        let likeAction = UIPreviewAction(title: "Like", style: .default) { (action, viewController) -> Void in
            print("You liked the photo")
        }
        
        let deleteAction = UIPreviewAction(title: "Delete", style: .destructive) { (action, viewController) -> Void in
            print("You deleted the photo")
        }
        
        return [likeAction, deleteAction]
        
    }
}
