

import UIKit
import CollapsibleTableSectionViewController
import SVProgressHUD

class CustomCellViewController: CollapsibleTableSectionViewController {
    
   // var sections: [Section] = sectionsData
    var policeSections = [policeSection] ()
    var asistanSections = [asistanSection] ()
    
    var selectedValue:String!
    let replication = MergeReplication()
    
    var alert = UIAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: Header Section
        if selectedValue == "Police" {
            policeSections.append(policeSection.init(genre: "", icerik: GlobalVariables.shared.policeListeleme![0], height: 250))
            
            var sayac = 0
            // TODO: Section info
            for i in GlobalVariables.shared.policeListeleme! {
                sayac += 1
                
                var genre = String(sayac) + (". ") + i.Urun!.temizle() + (" - ") + i.rizikoadres!.temizle() // Konut vs Diger
                
                if String(i.PoliceNo!).first == "2" || String(i.PoliceNo!).first == "2" { // Kasko ve Trafik
                    genre = String(sayac) + (". ") + i.Urun!.temizle() + (" - Plaka: ") + String(i.Plaka!)
                }
                
                policeSections.append(policeSection(genre: genre, icerik: i, height: 80))
                
            }
        } else if selectedValue == "Yardım"  {
            asistanSections.append(asistanSection.init(genre: "", asistanicerik: GlobalVariables.shared.asistanPoliceListeleme![0], height: 250))
            asistanSections.append(asistanSection.init(genre: "", asistanicerik: GlobalVariables.shared.asistanPoliceListeleme![0], height: 70))
            
            var sayac1 = 0
            
            for i in GlobalVariables.shared.asistanPoliceListeleme! {
                sayac1 += 1
                
                var genre = String(sayac1) + (" - Plaka: ") + i.Adres!.temizle() // Konut vs Diger
                
                if String(i.PoliceNo!).first == "2" || String(i.PoliceNo!).first == "2" { // Kasko ve Trafik
                    genre = String(sayac1) + (". ") + (" - Plaka: ") + String(i.Plaka!)
                }
                
                asistanSections.append(asistanSection(genre: genre, asistanicerik: i, height: 60))
                
            }
            
        }
        
        self.delegate = self
        
    }
    
}
public func stroke(font: UIFont, strokeWidth: Float, insideColor: UIColor, strokeColor: UIColor) -> [NSAttributedString.Key: Any]{
    return [
        NSAttributedString.Key.strokeColor : strokeColor,
        NSAttributedString.Key.foregroundColor : insideColor,
        NSAttributedString.Key.strokeWidth : -strokeWidth,
        NSAttributedString.Key.font : font
    ]
}

extension CustomCellViewController: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        
        if selectedValue == "Police"{
            return policeSections.count
        }
        else if selectedValue == "Yardım" {
            
            return asistanSections.count
        }
        
        return 0
        
    }
    
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1           //sections[section].items.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedValue == "Police"{
            return policeSections[section].height
        }
        else if selectedValue == "Yardım" {
            return asistanSections[section].height            
        }
        return 0
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 315 // cell row height
    }
    
    // baslık boyutu ve ıceriği
    func collapsibleTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 { // Header Image
            let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 250))
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 250))
            if selectedValue == "Police"{
                imageView.image = UIImage(named: "Police Sorgulama") }
            else {
                imageView.image = UIImage(named: "Yol Yardim")
            }
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            header.addSubview(imageView)
            
            // Flicker View
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 250))
            view.backgroundColor = UIColor.black
            view.alpha = 0.17
            header.addSubview(view)
            let basliklabel = UILabel(frame: CGRect(x:0, y:75.0, width: self.view.frame.width, height: 100))
            basliklabel.textAlignment = .center
            basliklabel.textColor = UIColor.black
            let attributes = stroke(font: UIFont(name: "Arial-BoldMT", size: 34)!, strokeWidth: 3.0, insideColor: .white, strokeColor: .black)
            if selectedValue == "Police"{
                basliklabel.attributedText = NSMutableAttributedString(string: "Poliçelerim",attributes: attributes) }
            else {
                basliklabel.attributedText = NSMutableAttributedString(string: "Yardım Hizmetleri",attributes: attributes)
            }
            header.addSubview(basliklabel)
            return header
        } else if section == 1 && selectedValue == "Yardım"{
            let yardimView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 70))
            let yardimlabel = UILabel(frame: CGRect(x:0, y:5, width: self.view.frame.width, height: yardimView.frame.height))
            yardimlabel.text = "Yardım talebi için ilgili poliçeyi seçin."
            yardimlabel.font = UIFont.boldSystemFont(ofSize: 20.0)
            yardimlabel.textColor = UIColor.hdiGreenColor()
            yardimlabel.textAlignment = .center
            yardimView.addSubview(yardimlabel)
            return yardimView
        }
        
        return nil
    }
    
    // gövde boyutu ve ıceriği
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CustomCell ??
            CustomCell(style: .default, reuseIdentifier: "Cell")
        
        cell.selectionStyle = .none
        
        if selectedValue == "Police"{
            
            let data = policeSections[indexPath.row].icerik!
            
            cell.view.nameAcente.text = "\(data.AcenteKod!)"
            
            let policestate = data.iptal
            
            if  policestate == "E" {
                cell.view.namePolDurm.text = "Yürürlükte"
                
            } else { cell.view.namePolDurm.text = "İptal"
            }
            
            // cell.view.nameYardim.isHidden = true
            cell.view.nameAcenteAd.text = data.AcenteAd?.temizle()
            cell.view.nameBsTarih.text = String(data.BaslangicTarihi!).toDateString(inputFormat: "dMMyyyy", outputFormat: "dd.MM.yyyy")
            cell.view.nameBtTarih.text = String(data.BitisTarihi!).toDateString(inputFormat: "dMMyyyy", outputFormat: "dd.MM.yyyy")
            cell.view.namePolice.text = String(data.PoliceNo!)
            
            cell.view.nameUrun.text = data.Urun!
            cell.view.nameZeyil.text = String(data.ZeyilNo!)
            
            if String(data.PoliceNo!).first == "2" || String(data.PoliceNo!).first == "2" { // Kasko ve Trafik
                cell.view.nameAdresPlaka.text = "Plaka:"
                cell.view.nameTextView.text = String(data.Plaka!)
            } else {
                cell.view.nameAdresPlaka.text? = "Adres:"
                cell.view.nameTextView.text = String(data.rizikoadres!.temizle())
            }
            
        } else if selectedValue == "Yardım" {
            
            let data = asistanSections[indexPath.row].asistanicerik!
            cell.view.nameAcente.text = "\(data.AcenteKod!)"
            
            
            let policestate = data.IptalDurumu
            
            if  policestate == "E" {
                cell.view.namePolDurm.text = "Yürürlükte"
                
            } else { cell.view.namePolDurm.text = "İptal"
            }
            
            // cell.view.nameYardim.isHidden = false
            cell.view.nameAcenteAd.text = data.AcenteAd?.temizle()
            cell.view.nameBsTarih.text = String(data.BaslangicTarihi!).toDateString(inputFormat: "dMMyyyy", outputFormat: "dd.MM.yyyy")
            cell.view.nameBtTarih.text = String(data.BitisTarihi!).toDateString(inputFormat: "dMMyyyy", outputFormat: "dd.MM.yyyy")
            cell.view.namePolice.text = String(data.PoliceNo!)
            
            cell.view.nameUrun.text = data.Urun!
            cell.view.nameZeyil.text = String(data.ZeyilNo!)
            
            //
            if String(data.PoliceNo!).first == "2" || String(data.PoliceNo!).first == "2" { // Kasko ve Trafik
                cell.view.nameAdresPlaka.text = "Plaka:"
                cell.view.nameTextView.text = String(data.Plaka!)
            } else {
                cell.view.nameAdresPlaka.text? = "Adres:"
                cell.view.nameTextView.text = String(data.Adres!.temizle())
            }
            
            let mailbutton = PassableUIButton()
            mailbutton.frame = CGRect(x:115, y: 260, width: 230, height: 40)
            mailbutton.backgroundColor = UIColor.hdiGreenColor()
            mailbutton.setTitle("Yardım talebi için tıklayın", for: .normal)
            mailbutton.addTarget(self, action: #selector(self.OnSend(_:)), for:.touchUpInside)
            mailbutton.params = data
            cell.addSubview(mailbutton)
            
            let telephonetextField = UITextField()
            telephonetextField.frame = CGRect(x:115, y: 260, width: 230, height: 20)
            telephonetextField.backgroundColor = UIColor.black
            
            // textFieldCreate()
            
        }
        
        return cell
    }
    
    @objc func textFieldChanged(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.red.cgColor
        textField.layer.borderWidth = 1.0
       // textField.text? = "5"
        
        
        if textField.text! == "0"  {
            textField.text = "5"
        }
        
        
        if textField.text?.first != "5"  {
            textField.text = "5" + textField.text!
        }
           

        
        if textField.text!.count > 9 {
            textField.layer.borderColor = UIColor.clear.cgColor
        }
        
        if textField.text!.count > 10 {
            textField.text!.removeLast()
        }
    }
    
    @objc func OnSend(_ sender: PassableUIButton) {
        var CustomerPhoneNumber = ""
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        alert = UIAlertController(title: "", message: "Telefon numaranızı giriniz.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            textField.addTarget(self, action: #selector(self.textFieldChanged(_:)), for: .editingChanged)
        }
        
        alert.addAction(UIAlertAction(title: "Gönder", style: .default, handler: {
            (action) -> Void in
            
            if let phoneTextfield = self.alert.textFields?.first {
                CustomerPhoneNumber  = phoneTextfield.text!
                // Maili Gönderme
                
                self.replication.PoliceBilgiMailGönderme(sender.params , MüsteriTelefonNo: CustomerPhoneNumber ) { (status) in
                    if status {
                        AlertFunctions.messageType.showOKAlert("Teşekkürler", bodyMessage: "Talebiniz başarıyla iletildi.")
                    } else {
                        AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Mailiniz gönderilemedi tekrar deneyiniz")
                    }
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Vazgeç", style: .default, handler: {
            (action) -> Void in
            
           
            self.alert.dismiss(animated: true, completion: nil)
            
        }))
        self.present(alert, animated : true, completion: nil)
        
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if selectedValue == "Police"{
            return policeSections[section].genre
            
        }
        else if selectedValue == "Yardım" {
            
            
            return asistanSections[section].genre
            
        }
        return nil
    }
    
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
    func shouldCollapseOthers(_ tableView: UITableView) -> Bool {
        return true
    }
    
    
    
    func collapsibleTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

class PassableUIButton: UIButton{
    var params: AsistanBilgi?
    
    override init(frame: CGRect) {
        self.params = nil
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.params = nil
        super.init(coder: aDecoder)
    }
}


