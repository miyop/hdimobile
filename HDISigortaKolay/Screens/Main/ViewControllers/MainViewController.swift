//
//  MainViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 9.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

//enum NetworkResponse<T: Any> {
//    case success(object: T)
//    case failure(error: Error, description: String)
//}
//
//
//let foo = NetworkResponse<String>.success(object: "Sema")
//
//switch foo {
//case .success(let object):
//    break
//case .failure(let error, let description)
//    break
//}

class MainViewController: UIViewController , UITextFieldDelegate {
    
    // MARK: - UI Elements
    @IBOutlet weak var connectionFailureLabel: UILabel!
    @IBOutlet weak var ImageHDIBanner: UIImageView!
    @IBOutlet weak var txtPolice: UITextField!
    @IBOutlet weak var imgMiniPolice: UIImageView!
    @IBOutlet weak var imgMiniUser: UIImageView!
    @IBOutlet weak var btnSorgula: UIButton!
    @IBOutlet weak var txtTcKimlikNo: UITextField!
    @IBOutlet weak var viewTcCizgi: UIView!
    @IBOutlet weak var txtInfo1: UILabel!
    @IBOutlet weak var txtInfo2: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewPoliceCizgi: UIView!
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presentTransparentNavigationBar(status: false)
        txtTcKimlikNo.delegate = self
        txtPolice.delegate = self
        self.txtTcKimlikNo.keyboardType = .numberPad
        self.txtPolice.keyboardType = .numberPad
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.activityIndicator.alpha = 0.0
        delay(0.1, closure: { () -> () in
            self.activityIndicator.stopAnimating()
        })
    }
    
    // MARK: - Actions
    @IBAction func txtTcVknEditChanged(_ sender: UITextField) {
        if sender.text!.count == 0 || sender.text!.count > 9 {
            txtInfo1.isHidden = true
            viewTcCizgi.alpha = 0.5
            viewTcCizgi.backgroundColor = UIColor.hdiGreenColor()
        } else {
            txtInfo1.isHidden = false
            viewTcCizgi.alpha = 1
            viewTcCizgi.backgroundColor = UIColor.red
        }
    }
    
    @IBAction func txtPoliceEditChanged(_ sender: UITextField) {
        if sender.text!.count == 0 || sender.text!.count > 11 {
            txtInfo2.isHidden = true
            viewPoliceCizgi.alpha = 0.5
            viewPoliceCizgi.backgroundColor = UIColor.hdiGreenColor()
        } else {
            txtInfo2.isHidden = false
            viewPoliceCizgi.alpha = 1
            viewPoliceCizgi.backgroundColor = UIColor.red
        }
    }
    
    // MARK: - Functions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtPolice.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if textField == txtTcKimlikNo {
            let newLength = text.count + string.count - range.length
            return newLength < 12 // Bool
        } else {
            let newLength = text.count + string.count - range.length
            return newLength < 13 // Bool
        }
    }
    
    func loadGreetingView() {
        if Reachability().isConnectedToNetwork() {
        } else {
            self.showTryAgainState()
        }
    }
    
    func showTryAgainState() {
        //self.connectionFailureLabel.text = "İnternet Bağlantınızı kontrol edin."
        AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "İnternet Bağlantınızı kontrol edin.")
        self.activityIndicator.alpha = 0.0
    }
    
    @IBAction func btnSorgula(_ sender: Any) {
        
        loadGreetingView()
        
        var tckvno = Int(txtTcKimlikNo.text!)
        var polno = Int(txtPolice.text!)
        
        //textFielddan girilen değerin vergi yada tc oldugunu verir.
        let vergimitcmmi = txtTcKimlikNo.textFieldLength()
        
        // FIXME: Kaldırılacak
       // tckvno = 18155900830
        //polno = 520121002961
        tckvno = 10058394358
        polno = 266071005061
        
        if (tckvno != nil) && (polno != nil) {
            
            //Poliçe listesi veren web servis 
            MergeReplication.init().PoliceListeleme(tckvno!, type: "tc") { (response, error) in
                if error == nil {
                    if response?.count != 0 {
                        GlobalVariables.shared.policeListeleme = response
                    }
                }
            }
            
            //
            MergeReplication.init().AsistanPoliceListeleme(tckvno!, type: "tc") { (response, error) in
                
                
                if error == nil {
                    if response?.count != 0 {
                        GlobalVariables.shared.asistanPoliceListeleme = response
                    }
                }
            }
            // self'in nil olma durumunda strongSelf kullanılacak. Diğer türlü fonksiyon çalışmayacak.
            MergeReplication.init().PoliceBilgiGetir(polno!, tckimlik: tckvno!, type: vergimitcmmi) { [weak self] (response, error) in
                guard let strongSelf = self else { return }
                
                if error == nil {
                    if response?.count != 0 {
                        GlobalVariables.shared.policebilgi = response
                        // AlertFunctions.messageType.showOKAlert("Police Alindi", bodyMessage: "count: \(response.count) AcenteAd: " + response[0].AcenteAd!)
                        strongSelf.performSegue(withIdentifier: "toProductViewController", sender: nil)
                    } else {
                        AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Girilen TC Kimlik No ve Poliçe No eşleşmemektedir.")
                    }
                } else {
                    AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Bir sorun oluştu, daha sonra tekrar deneyin.")
                }
                SVProgressHUD.show()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                delay(0.8, closure: { () -> () in
                    SVProgressHUD.dismiss()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
        } else {
            AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Lütfen boş alan bırakmayın.")
        }
    }
}

