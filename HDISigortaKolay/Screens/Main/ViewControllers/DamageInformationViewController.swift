//
//  DamageInformationViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 7.07.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//

import Foundation
import UIKit

class DamageInformationViewController: UIViewController {
    
    var selectedValue:String!
    
    @IBOutlet weak var txtDamageInformation: UITextField!
    
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBAction func btnQueries(_ sender: Any) {
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MergeReplication.init().HasarBilgiListeleme(10058394358, dosyaNo: 20180017580) { (response, error) in
            if error == nil {
                print(response!)
            } else {
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
            }
        }
        
    }
    
}
