//
//  GreetingViewController.swift
//  HDISigortaKolay
//
//  Created by Sema Sancar on 17.05.2018.
//  Copyright © 2018 HDI Sigorta. All rights reserved.
//


import UIKit

class GreetingViewController: UIViewController {
    
    
    @IBOutlet weak var hdiText: UILabel!
    @IBOutlet weak var imgHdiLogo: UIImageView!
    
    
    @IBOutlet weak var hdicizgi: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationStart()
        animationNext()
    }
    
    func animationStart()  {
        
        // orjinal konumu
        let hdiTextBound = hdiText.frame
        let hdilogoBound = imgHdiLogo.frame
        
        // Start noktasında olması gereken konum
        self.hdiText.frame.origin.y = self.view.frame.height
        self.imgHdiLogo.frame.origin.y = self.imgHdiLogo.frame.height * -1
        
        UIView.animate(withDuration: 1.4, delay: 0.25, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.45, options: [], animations: {
            // Finish noktasında olması gereken konum
            self.hdiText.frame.origin.y = hdiTextBound.origin.y
            self.imgHdiLogo.frame.origin.y = hdilogoBound.origin.y
        }, completion: nil)
    }
    
    func animationNext()  {
        
        let imgtrans = self.hdicizgi.frame
        
        self.hdicizgi.frame.origin.x = self.hdicizgi.frame.height * -1
        self.hdicizgi.frame.origin.y = self.hdicizgi.frame.height * -1
        
        
        UIView.animate(withDuration: 4, animations: {
            self.hdicizgi.frame.origin.x = imgtrans.origin.x
            self.hdicizgi.frame.origin.y = imgtrans.origin.y
            
            // Finish noktasında olması gereken konum
        }) { (status) in
            // animasyon bittikten sonra
            self.performSegue(withIdentifier: "open", sender: nil)
        }
        
    }
    
}

























